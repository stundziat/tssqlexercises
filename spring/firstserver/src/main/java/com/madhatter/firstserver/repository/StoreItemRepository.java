package com.madhatter.firstserver.repository;

import com.madhatter.firstserver.model.StoreItem;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreItemRepository extends CrudRepository<StoreItem, Long>
{
}
