INSERT INTO album(id, artist, date_released, sold, album_name)
VALUES (null, 'artist', 1890, 3700, 'rock_album');

INSERT INTO album(id, artist, date_released, sold, album_name)
VALUES (100, 'Opeth', 1900, 10000000, 'metal');

INSERT INTO artists(id, names, country, birthday)
VALUES (null, 'Slipknot', 'United States', '07/11/1973');

INSERT INTO album_artists(id, albums_id, artists_id)
VALUES (null, 2, 1);

SELECT * FROM album;
SELECT * FROM artists;
