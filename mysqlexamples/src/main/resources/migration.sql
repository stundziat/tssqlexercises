# MAKE A TABLE FOR ALBUMS
# ARTIST, DATE RELEASED, SOLD, ALBUM NAME
# Make a table for artists: Many to Many with albums
#   names, country, birthday
# Make a table for concerts: Many to Many with artists
#   tour, date

SHOW TABLES;

CREATE TABLE album(id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                  artist varchar(30) NOT NULL,
                  date_released varchar(30) NOT NULL,
                  sold INT UNSIGNED NOT NULL,
                  album_name varchar(30) NOT NULL,
                  CONSTRAINT pk_album primary key(id));

CREATE TABLE artists(id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                     names varchar(30) NOT NULL,
                     country varchar(30) NOT NULL,
                     birthday varchar(30) NOT NULL,
                     CONSTRAINT pk_artists primary key(id));

CREATE TABLE album_artists(id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                           albums_id INT UNSIGNED,
                           artists_id INT UNSIGNED,
                           CONSTRAINT PRIMARY KEY(id),
                           FOREIGN KEY(albums_id) REFERENCES album(id),
                           FOREIGN KEY(artists_id) REFERENCES artists(id));

CREATE TABLE artists_concerts(id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                              tour varchar(30) NOT NULL,
                              tour_date varchar(30) NOT NULL,
                              FOREIGN KEY(id) REFERENCES artists(id));
