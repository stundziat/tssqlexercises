package com.mongodb.mongodb.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class Employee
{
    // ***** Member variables *****

    @Id
    private String id;

    private String first_name;

    private String last_name;

    // ***** Constructors *****

    public Employee()
    {
        first_name = "Tomas";
        last_name = "Stundzia";
    }

    public Employee(String first_name, String last_name)
    {
        this.first_name = first_name;
        this.last_name = last_name;
    }

}
