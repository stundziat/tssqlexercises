package com.mongodb.mongodb.controller;

import com.mongodb.mongodb.model.Employee;
import com.mongodb.mongodb.repository.EmployeeRepo;
import com.mongodb.mongodb.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController
{
    // Make these final once you know how to initialize them
    // Autowired used for when you have a custom datatype - it can be variables or methods
    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/aPath")
    public List<Employee> getEmployees()
    {
        employeeService.insertEmployee();
        return employeeRepo.findAll();
    }

    @GetMapping("/bPath")
    public Employee returnEmployee()
    {
        return employeeService.getBat();
    }

    @GetMapping("/cPath")
    public Employee returnSuperman() { return employeeService.getSuperman(); }
}
