
# Basic SQL
# a) Select all departments
SELECT * FROM departments;

# b) Select all titles
SELECT * FROM titles;

# c) Select all employees hired after a certain date
SELECT * FROM employees WHERE hire_date > '1985-01-01;';
SELECT * FROM employees WHERE employees.employees.hire_date > '1985-01-01';

# d) Select all employees with a certain first name
SELECT * FROM employees WHERE first_name = 'Kyoichi';

# e) Select all employees with a certain first name and over a certain birth date
SELECT * FROM employees WHERE first_name = 'Kyoichi' AND birth_date > '1955-04-01';

# Advanced SQL
# a) Select all male employees with distinct first names
SELECT DISTINCT first_name FROM employees.employees WHERE gender = 'M';

# b) Select alphabetical list of employees grouped by department
SELECT e.first_name, d.dept_no FROM employees as e
INNER JOIN dept_emp as d on e.emp_no = d.emp_no
ORDER BY d.dept_no, e.first_name;

# c) Select all employees who make a certain amount in a certain department
SELECT e.emp_no FROM employees as e
INNER JOIN dept_emp as d on e.emp_no = d.emp_no
INNER JOIN salaries as s on e.emp_no = s.emp_no
INNER JOIN departments as dd on d.dept_no = dd.dept_no
WHERE salary > 120000 AND dept_name = 'Marketing';

# d) Select all employees who make over a certain amount grouped by department
SELECT * FROM employees as e
INNER JOIN salaries s on e.emp_no = s.emp_no
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
WHERE s.salary > 150000 AND s.to_date LIKE '9999%'
ORDER BY d.dept_name;

# 3) Advanced SQL Exercise 2 *****

# a) Select a limited set of employees in a department
SELECT * FROM employees as e
INNER JOIN dept_emp as de on e.emp_no = de.emp_no
INNER JOIN departments as d on de.dept_no = d.dept_no
WHERE e.gender = 'M' AND e.hire_date > '2000-01-01'
LIMIT 3;

# b) Select a limited set of employees in a department that make a salary range
SELECT * FROM employees as e
INNER JOIN dept_emp as de on e.emp_no = de.emp_no
INNER JOIN departments as d on de.dept_no = d.dept_no
INNER JOIN salaries as s on e.emp_no = s.emp_no
WHERE d.dept_name = 'Research' AND s.salary BETWEEN 110000 AND 130000
LIMIT 15 OFFSET 5;

# c) Select a limited set of employees that are in an age range
SELECT e.first_name, e.last_name, e.birth_date FROM employees as e
WHERE e.birth_date > '1960-01-01'
LIMIT 10;

# d) Select a limited set of the oldest employees
SELECT * FROM employees as e
WHERE e.birth_date < '1960-01-01' AND e.gender = 'F'
LIMIT 2;


# 4) Advanced SQL Exercise 3 *****

# a) Show the names of all department managers and their departments
SELECT e.first_name, e.last_name, d.dept_name FROM employees as e
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
INNER JOIN departments d on dm.dept_no = d.dept_no
ORDER BY e.emp_no;

# b) Return an employee with their job title and salary
SELECT e.first_name, e.last_name, t.title, s.salary FROM employees as e
INNER JOIN salaries s on e.emp_no = s.emp_no
INNER JOIN titles t on e.emp_no = t.emp_no
WHERE t.title = 'Senior Engineer' AND s.salary > 120000
LIMIT 1;

# c) Return a limited set of the best paid employees grouped by department manager
SELECT DISTINCT e.first_name, e.last_name, d.dept_name, s.salary FROM employees as e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
INNER JOIN dept_manager dm on d.dept_no = dm.dept_no
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE de.dept_no = 'd008' AND dm.emp_no = '111400' AND s.salary > 120000
LIMIT 10;

# d) Return all employees making salaries within two ranges using MINUS (or union)
SELECT DISTINCT e.first_name, e.last_name, s.salary FROM employees e
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE s.salary BETWEEN 79998 AND 80000
UNION
SELECT DISTINCT e2.first_name, e2.last_name, s2.salary FROM employees e2
INNER JOIN salaries s2 on e2.emp_no = s2.emp_no
WHERE s2.salary BETWEEN 145000 AND 149000
LIMIT 300;

# 5) Advanced SQL Exercise 4

# a) Order a list of employees with the same name by job title
SELECT e.first_name, e.last_name, t.title FROM employees as e
INNER JOIN titles as t on e.emp_no = t.emp_no
WHERE e.first_name = 'Lidong'
ORDER BY t.title;

# b) Select a list of employees with the same name ordered by salary
SELECT e.first_name, e.last_name, s.salary FROM employees as e
INNER JOIN salaries as s on e.emp_no = s.emp_no
WHERE e.first_name = 'Bojan' AND e.last_name = 'Montemayor'
ORDER BY s.salary;

# c) Select a limited set of the most senior employees in a department
SELECT e.first_name, e.last_name, e.birth_date, d.dept_name FROM employees as e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
ORDER BY e.birth_date
LIMIT 100;

# d) Select a list of employees ordered by birth date
SELECT * FROM employees as e
WHERE e.birth_date > '1960-12-01'
ORDER BY e.birth_date
LIMIT 100;

# 6) Advanced SQL Exercise 5

# a) Select the department managers ordered by seniority
SELECT e.first_name, e.last_name, dm.from_date FROM employees as e
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
INNER JOIN departments d on dm.dept_no = d.dept_no
ORDER BY dm.from_date;

# b) Select employees who have been with the company for a certain time
#    and have birthdays in a certain month
SELECT e.first_name, e.last_name, e.hire_date, e.birth_date FROM employees as e
WHERE e.birth_date LIKE '____-12-__' AND e.hire_date > '1990-01-01';

# c) Select the departments that have a department manager of one gender
SELECT DISTINCT e.first_name, e.last_name, e.gender, d.dept_name FROM employees as e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on d.dept_no = de.dept_no
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
WHERE e.gender = 'M';

# d) Select the highest paid employee in a department
SELECT e.first_name, e.last_name, s.salary, d.dept_name FROM employees as e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE s.salary > 100000 and d.dept_name = 'Research'
ORDER BY s.salary
LIMIT 1;

# 7) SubQuery Exercise 1 *****

# a) Select the names of employees who have reported to a specific department
#    manager in the last year
SELECT e.first_name, e.last_name, de.to_date, dm.emp_no FROM employees as e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN dept_manager dm on de.dept_no = dm.dept_no
WHERE de.dept_no = 'd001' AND dm.emp_no = '110039' AND de.to_date = '9999-01-01';

# b) Select the names of employees who have received a pay increase in the last year
# ????
# SELECT e.first_name, e.last_name FROM employees as e
# INNER JOIN dept_emp de on e.emp_no = de.emp_no
# INNER JOIN salaries s on e.emp_no = s.emp_no
# WHERE s.to_date = '9999-01-01';
select s.emp_no, e.first_name, last_name, s.salary, s.from_date, s.year from employees e
inner JOIN
(
 select s.salary, s.to_date, s.emp_no, s.from_date, year(s.from_date) as year from salaries s
  WHERE
        (
          SELECT year(s.from_date) as year FROM salaries s
          WHERE s.to_date != '9999-01-01'
          GROUP BY year
          ORDER BY year desc
          LIMIT 1
              )
     ) s on e.emp_no
WHERE s.year = '2001';

# c) Select the name of the highest paid employee in the largest department
SELECT s.salary FROM employees as e
INNER JOIN salaries s on e.emp_no = s.emp_no
GROUP BY s.salary
ORDER BY MAX(s.salary) DESC;
# WHERE e.first_name = 'Georgi' AND d.dept_name = 'Research';

# d) Order a historical department managers for a department
#    based on how long they held the position

SELECT e.first_name, e.last_name FROM employees as e
WHERE e.emp_no IN
      (SELECT dm.emp_no FROM dept_manager as dm WHERE dm.emp_no > '111000');

# SELECT e.first_name FROM employees as e
# WHERE e.first_name IN
#       (SELECT dm.from_date FROM dept_manager as dm WHERE dm.from_date < '1999-01-01' ORDER BY dm.from_date);
#
SELECT e.first_name FROM employees as e
WHERE e.emp_no IN
      (SELECT dm.emp_no FROM dept_manager as dm
      INNER JOIN dept_emp as de on dm.emp_no = de.emp_no
      INNER JOIN departments as d on dm.dept_no = d.dept_no
      WHERE dm.emp_no > '111111' ORDER BY dm.from_date);

SELECT * FROM departments as d
INNER JOIN dept_manager dm on dm.dept_no = d.dept_no
WHERE d.dept_no IN
      (SELECT dm.from_date FROM dept_manager as dm
WHERE dm.from_date < '1999-01-01' ORDER BY dm.from_date);

SELECT e.first_name, e.last_name, dm.emp_no, dm.dept_no, dm.to_date, dm.from_date, datediff(dm.to_date, dm.from_date) as diff FROM employees as e
INNER JOIN (select dm.emp_no, dm.dept_no, dm.from_date, CASE WHEN dm.to_date = '9999-01-01' then date_add(dm.to_date, interval -7997 year) when dm.to_date != '9999-01-01' then dm.to_date END as to_date from
dept_manager dm) dm on e.emp_no = dm.emp_no;




