"use strict";

console.log(3);
let x=3;
console.log(3+x);

// function add(a, b)
// {
//     return a + b;
// }
//
// let multiply = (c, d) => {return c * d;};
//
// console.log('Function calls');
// console.log(add(2, 5));
// console.log(multiply(1, 20));
//
// const pi=3.14;


// 1) Create function that checks for odd
function isOdd(num1)
{

    let is_odd = num1 % 2;

    return is_odd !== 0;
}


// 2) Math functions

function add(a, b) { return a + b; }
function subtract(a, b) { return a - b; }
function multiply(a, b) { return a * b; }
function divide(a, b) { if (b !== 0) {return a / b;} }
function modulus(a, b) { return a % b; }


// 3) Hello World

function helloWorld(your_name)
{
    return "Hello " + your_name;
}

let prompt = (name) => {return "Hello " + name};


// 4) Test for positive number

function isPositive(a)
{
    return a >= 0;
}


// 5) Test for equality of strings

function isMatchStrings(string1, string2)
{
    return string1 === string2;
}


// Challenge: FizzBuzz

function fizzbuzz(N)
{
    for (let i=0; i < N; i++)
    {
        console.log("N: ", i);
        if ((i % 3 === 0) && (i % 5 === 0))
        {
            console.log("FizzBuzz");
        }
        else if ( i % 3 === 0 ) { console.log("Fizz") }
        else if ( i % 5 === 0 ) { console.log("Buzz") }
    }
}


// TESTING

console.log("Is Odd:");
console.log(isOdd(7));
console.log(isOdd(4));

console.log("Math:");
console.log(add(10, 10));
console.log(subtract(10, 10));
console.log(multiply(10, 10));
console.log(divide(10, 10));
console.log(divide(10, 0));

console.log("Name:");
console.log(helloWorld("Tom"));
console.log(prompt("Tom"));

console.log("Positive:");
console.log(isPositive(10));
console.log(isPositive(-10));

console.log("Matching Strings:");
console.log(isMatchStrings("python", "c++"));
console.log(isMatchStrings("python", "python"));

console.log("Fizz Buzz:");
console.log(fizzbuzz(60));
